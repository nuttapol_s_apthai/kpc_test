module.exports = class OutputHandler {
    stdout(message) {
        process.stdout.write(`${message}\n`);
    }

    stderr(message) {
        process.stderr.write(`${message}\n`);
    }
}