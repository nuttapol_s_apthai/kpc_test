module.exports = Object.freeze({
	CREATE_PARKING_LOT: 'createParkingLot',
	PARK: 'park',
	LEAVE: 'leave',
	STATUS: 'status',
	EXIT: 'exit',
	REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR: 'registrationNumbersForCarsWithColour',
	SLOT_NUMBERS_FOR_CARS_WITH_COLOUR: 'slotNumbersForCarsWithColour',
	SLOT_NUMBER_FOR_REGISTRATION_NUMBER: 'slotNumberForRegistrationNumber'
});