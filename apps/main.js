const readline = require('readline');
const fs = require("fs");

const { GarageController } = require('./garage');
const OutputHandler = require('./helpers/outputHandler.js');

const COMMAND_ENUMS = require('./config/commandEnums.js')

class Main {
  constructor() {
    this.inputArgs = process.argv.slice(2);

    this.garage = new GarageController();
    this.output = new OutputHandler();

    this.path = this.inputArgs[0]?.split('/').length === 1 ? `./bin/${this.inputArgs[0]}` : this.inputArgs[0];
    this.hasFileArgs = this.inputArgs?.length > 0;

    this.rl = readline.createInterface({
      input: this.hasFileArgs ? fs.createReadStream(this.path) : process.stdin,
      output: this.hasFileArgs ? null : process.stdout,
    });

    this.commandMapping = {
      create_parking_lot: COMMAND_ENUMS.CREATE_PARKING_LOT,
      park: COMMAND_ENUMS.PARK,
      leave: COMMAND_ENUMS.LEAVE,
      status: COMMAND_ENUMS.STATUS,
      exit: COMMAND_ENUMS.EXIT,
      registration_numbers_for_cars_with_colour: COMMAND_ENUMS.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR,
      slot_numbers_for_cars_with_colour: COMMAND_ENUMS.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR,
      slot_number_for_registration_number: COMMAND_ENUMS.SLOT_NUMBER_FOR_REGISTRATION_NUMBER
    };
  }

  start() {
    try {
      this.rl.on("line", (input) => {
        const [commandInput, ...arg] = input.split(' ');

        const commandMapped = this.commandMapping[commandInput];

        if (commandMapped === COMMAND_ENUMS.EXIT) {
          return this.rl.close();
        }
  
        const command = this.garage[commandMapped] ?? null;

        if (command) {
          command(arg);
        } else {
          this.output.stdout(`Command '${commandInput}' not found`);
        }
      });
    } catch (error) {
      this.output.stdout(`${error.message}\n`);
    }
  }
}

(function main() {
  new Main().start();
})();
