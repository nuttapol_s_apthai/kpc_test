module.exports = class Car {
    constructor(license_plate, color) {
      this.license_plate = license_plate;
      this.color = color;
    }
  
    toString() {
      return `${this.license_plate} ${this.color}`;
    }
  }
  