module.exports = class Garage {
    constructor() {
      this.spots = 0;
      this.identifier = [];
      this.carInfo = {
        code: [],
        license_plate: [],
        color: []
      };
    }
  
    createParkingLot(lot) {
      const numberOnly = /^[0-9]*$/;
      if (!lot || lot <= 0 || !numberOnly.test(lot)) throw new Error('Invalid lot size');
  
      this.spots = lot;
      this.identifier = Array.from({ length: lot }, (_, i) => String(i + 1));
      return this.identifier.length;
    }
  
    spotsAvailable() {
      return this.spots;
    }
  
    addCar(car = '') {
      if (!car) throw new Error('Invalid input format');
      if (this.spots <= 0) throw new Error('Sorry, parking lot is full');
  
      const [licensePlate, color] = String(car).split(/\s+/).filter(Boolean);
     
      if (!licensePlate || !color) throw new Error('Invalid input format');
  
      // Check duplicate car in garage
      const duplicateCarLicensePlate = this.carInfo.license_plate.includes(licensePlate);
      const duplicateCarColorPlate = this.carInfo.license_plate.includes(color);
    
      if (duplicateCarLicensePlate && duplicateCarColorPlate) {
        throw new Error('Has this car in garage');
      }
      
      const spotNumber = this.identifier[0];
    
      // Added car to card info
      this.carInfo.code.push(spotNumber);
      this.carInfo.license_plate.push(licensePlate);
      this.carInfo.color.push(color);
  
      // Remove identifier slot
      this.identifier.splice(0, 1);
      this.spots -= 1;
  
      return spotNumber;
    }
  
    removeCar(givenCode) {
      if (!this.carInfo.code.includes(givenCode)) {
        throw new Error(`No car found with code ${givenCode}`);
      }
  
      const carIndex = this.carInfo.code.findIndex((code) => code === givenCode)
  
      if (carIndex === -1) {
        throw new Error(`No car found with code ${givenCode}`);
      }
  
      this.carInfo.code.splice(carIndex, 1);
      this.carInfo.license_plate.splice(carIndex, 1);
      this.carInfo.color.splice(carIndex, 1);
  
      this.identifier.push(givenCode);
      this.spots += 1;
  
      return givenCode;
    }
  
    carsInGarage() {
      const header = 'Slot No.    Registration No    Colour';
  
      const carInfoArray = this.carInfo.code.map((code, index) => {
        const licensePlate = this.carInfo.license_plate[index];
        const color = this.carInfo.color[index];
        return `${code}           ${licensePlate}      ${color}`;
      });
  
      return [header, ...carInfoArray];
    }
    
    registrationNumbers(givenColor) {
      if (!this.carInfo.color.includes(givenColor)) {
        throw new Error(`Not found`);
      }
  
      let summary = [];
  
      for (let index = 0; index < this.carInfo.color.length; index++) {
        if (this.carInfo.color[index] === givenColor) {
          summary.push(this.carInfo.license_plate[index]);
        }
      }
  
      return summary.join(', ');
    }
  
    slotNumberColor(givenColor) {
      if (!this.carInfo.color.includes(givenColor)) {
        throw new Error(`Not found`);
      }
  
      let summary = [];
  
      for (let index = 0; index < this.carInfo.color.length; index++) {
        if (this.carInfo.color[index] === givenColor) {
          summary.push(this.carInfo.code[index]);
        }
      }
      return summary.join(', ');
    }
  
    slotNumberPlate(givenCode) {
      if (!this.carInfo.license_plate.includes(givenCode)) {
        throw new Error('Not found');
      }
  
      const carLicensePlateIndex = this.carInfo.license_plate.findIndex((license_plate) => license_plate === givenCode)
      return this.carInfo.code[carLicensePlateIndex] ?? null;
    }
  }