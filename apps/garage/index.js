const GarageService = require('./garageService.js');
const GarageController = require('./garageController.js');

module.exports = {
  GarageService,
  GarageController
}