const GarageService = require('./garageService.js');
const { CarService } = require('../car');
const OutputHandler = require('../helpers/outputHandler.js');

module.exports = class GarageController {
  constructor() {
    this.garageService = new GarageService();
    this.output = new OutputHandler();
  }

  createParkingLot = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }

    try {
      const slots =  this.garageService.createParkingLot(input[0]);
      this.output.stdout(`Created a parking lot with ${slots} slots`); 
    } catch (error) {
      this.output.stdout(`${error.message}`); 
    }
  }

  park = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }
    
    const [licensePlate, color] = input;

    try {    
      const slotNumber =  this.garageService.addCar(new CarService(licensePlate, color));
      this.output.stdout(`Allocated slot number: ${slotNumber}`);
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }
  }

  leave = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }

    const [spotNumber] = input;

    try {
      const result =  this.garageService.removeCar(spotNumber)
      this.output.stdout(`Slot number ${result} is free`);
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }
  }

  status = () => {
    try {
      const results =  this.garageService.carsInGarage();
      results.map((result) => this.output.stdout(`${result}`));
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }
  }

  registrationNumbersForCarsWithColour = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }

    const [carRegistration] = input; 

    try {
      const result =  this.garageService.registrationNumbers(carRegistration);
      this.output.stdout(`${result}`);
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }
  }

  slotNumbersForCarsWithColour = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }

    const [carColor] = input;
    
    try {
      const result =  this.garageService.slotNumberColor(carColor);
      this.output.stdout(`${result}`)
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }    
  }

  slotNumberForRegistrationNumber = (input = []) => {
    if (input.length === 0) {
      this.output.stdout('Not Input');
    }

    const [carColor] = input; 

    try {
      const result =  this.garageService.slotNumberPlate(carColor);
      this.output.stdout(`${result}`);
    } catch (error) {
      this.output.stdout(`${error.message}`);
    }
  }
}