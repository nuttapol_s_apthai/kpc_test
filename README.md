# Parking Lot with Nodejs and Docker

---

## Folder Structure

``` readme.md
├── apps
│   ├── car
│   │   ├── carService.js
│   │   ├── index.js
│   ├── config
│   │   ├── commandEnums.js
│   ├── garage
│   │   ├── garageController.js
│   │   ├── garageService.js
│   │   ├── index.js
├── bin
│   ├── check_node
│   ├── file_inputs.txt
│   ├── parking_lot
│   ├── run_functional_tests
│   ├── setup
├── functional_spec
│   ├── fixtures
│   │   ├── file_input.txt
│   ├── spec
│   │   ├── end_to_end_spec.rb
│   │   ├── parking_lot_spec.rb
│   │   ├── spec_helper.rb
│   ├── .rspec_status
│   ├── Gemfile
│   ├── Gemfile.lock
│   ├── Rakefile
│   ├── README.md
├── Dockerfile
└── README.md
```

## System Requirement

- Docker with nodejs (v.16.x), ruby 3.x and bash

## How to Run

### Step 1 Build and Run Docker container

1. Run `docker build -t parking_log_nodejs .` on root directory to build docker image
2. Run `docker run -d -it --name parking_log parking_log_nodejs` on root directory to run docker container

### Step 2 Execute program on Docker container

1. Run `docker exec -it parking_log bash` to execute in side docker container
2. Run `./bin/setup` in terminal to install dependencies and run unit tests
3. Run `./bin/parking_lot {file name}` in terminal to run program in file mode. Example `./bin/parking_lot file_inputs.txt`
4. Run `./bin/parking_lot` without input file in terminal to run program in interactive mode.

## How to run tests

1. Run `./bin/run_functional_tests` in terminal to run unit tests.
