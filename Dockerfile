FROM alpine:3.14

# Install Bash
RUN apk add --update bash

# Install Node.js and npm
RUN apk add --update nodejs npm

# Install Ruby and build dependencies
RUN apk add --update ruby ruby-dev

# Set the working directory
WORKDIR /app

# Copy the application files
COPY . .

# Install ruby dependencies
RUN cd functional_spec && gem install bundler:1.16.2

# Change fole permission
RUN chmod +x ./bin/*
